package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.Reservation;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-23T22:06:43")
@StaticMetamodel(Customer.class)
public class Customer_ { 

    public static volatile SingularAttribute<Customer, Character> gender;
    public static volatile ListAttribute<Customer, Reservation> reservations;
    public static volatile SingularAttribute<Customer, String> city;
    public static volatile SingularAttribute<Customer, Integer> phone;
    public static volatile SingularAttribute<Customer, String> name;
    public static volatile SingularAttribute<Customer, Long> id;
    public static volatile SingularAttribute<Customer, String> state;
    public static volatile SingularAttribute<Customer, String> addr;
    public static volatile SingularAttribute<Customer, String> nric;
    public static volatile SingularAttribute<Customer, String> email;

}