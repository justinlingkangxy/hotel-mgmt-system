package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.Reservation;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-23T22:06:43")
@StaticMetamodel(Room.class)
public class Room_ { 

    public static volatile ListAttribute<Room, Reservation> reservations;
    public static volatile SingularAttribute<Room, String> name;
    public static volatile SingularAttribute<Room, Long> id;
    public static volatile SingularAttribute<Room, Character> status;

}