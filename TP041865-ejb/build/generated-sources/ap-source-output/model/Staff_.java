package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.Payment;
import model.Reservation;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-23T22:06:43")
@StaticMetamodel(Staff.class)
public class Staff_ { 

    public static volatile SingularAttribute<Staff, Character> role;
    public static volatile SingularAttribute<Staff, Character> gender;
    public static volatile SingularAttribute<Staff, String> city;
    public static volatile SingularAttribute<Staff, String> nric;
    public static volatile ListAttribute<Staff, Reservation> reservations;
    public static volatile SingularAttribute<Staff, Integer> phone;
    public static volatile SingularAttribute<Staff, String> name;
    public static volatile SingularAttribute<Staff, Payment> payment;
    public static volatile SingularAttribute<Staff, Long> id;
    public static volatile SingularAttribute<Staff, String> state;
    public static volatile SingularAttribute<Staff, String> pwd;
    public static volatile SingularAttribute<Staff, String> addr;
    public static volatile SingularAttribute<Staff, String> email;

}