package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.Customer;
import model.Payment;
import model.Room;
import model.Staff;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-23T22:06:43")
@StaticMetamodel(Reservation.class)
public class Reservation_ { 

    public static volatile SingularAttribute<Reservation, String> check_out_date;
    public static volatile SingularAttribute<Reservation, Staff> staff;
    public static volatile SingularAttribute<Reservation, Payment> payment;
    public static volatile SingularAttribute<Reservation, Long> id;
    public static volatile SingularAttribute<Reservation, String> book_date;
    public static volatile SingularAttribute<Reservation, Customer> cust;
    public static volatile SingularAttribute<Reservation, Room> room;
    public static volatile SingularAttribute<Reservation, String> check_in_date;
    public static volatile SingularAttribute<Reservation, Character> status;

}