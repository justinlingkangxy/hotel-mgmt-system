package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.Reservation;
import model.Staff;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-23T22:06:43")
@StaticMetamodel(Payment.class)
public class Payment_ { 

    public static volatile SingularAttribute<Payment, Reservation> r;
    public static volatile SingularAttribute<Payment, Staff> s;
    public static volatile SingularAttribute<Payment, String> date_paid;
    public static volatile SingularAttribute<Payment, Double> paid;
    public static volatile SingularAttribute<Payment, Long> id;

}