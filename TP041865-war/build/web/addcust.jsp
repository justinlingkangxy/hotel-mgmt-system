<%-- 
    Document   : addcust
    Created on : Sep 19, 2017, 8:46:38 PM
    Author     : TP041865
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add Customer Page</title>
    </head>
    <body>
        <jsp:include page="reshome.jsp" />
        <br/><br/>
        <form action="Addcust" method="POST">
            <table>
                <tr>
                    <td>
                        NRIC:
                    </td> 
                    <td>
                        <input type="text" name="nric" size="20" maxlength="12" required> *without "-"
                    </td>
                </tr>
                <tr>
                    <td>
                        Name:
                    </td> 
                    <td>
                        <input type="text" name="name" size="20" required>
                    </td>
                </tr>
                <tr>
                    <td>
                        Gender:
                    </td>
                    <td>
                        <select name="gender">
                            <option value="M">Male</option>
                            <option value="F">Female</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        Phone (+60):
                    </td> 
                    <td>
                        <input type="number" name="phone" size="20" maxlength="10" required>
                    </td>
                </tr>
                <tr>
                    <td>
                        Email:
                    </td> 
                    <td>
                        <input type="email" name="email" size="25" required>
                    </td>
                </tr>
                <tr>
                    <td>
                        Address:
                    </td> 
                    <td>
                        <input type="text" name="addr" size="25" required>
                    </td>
                </tr>
                <tr>
                    <td>
                        City:
                    </td> 
                    <td>
                        <input type="text" name="city" size="20" required>
                    </td>
                </tr>
                <tr>
                    <td>
                        State:
                    </td> 
                    <td>
                        <input type="text" name="state" size="20" required>
                    </td>
                </tr>
            </table>
            <p>
                <input type="submit" value="Submit">
            </p>
        </form>
    </body>
</html>
