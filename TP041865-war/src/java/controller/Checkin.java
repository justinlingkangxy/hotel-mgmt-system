/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Reservation;
import model.ReservationFacade;
import model.Room;
import model.RoomFacade;

/**
 *
 * @author TP041865
 */
@WebServlet(name = "Checkin", urlPatterns = {"/Checkin"})
public class Checkin extends HttpServlet {

    @EJB
    private RoomFacade roomFacade;

    @EJB
    private ReservationFacade reservationFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String id = request.getParameter("id");
        
        List<Reservation> bookList = reservationFacade.findAll();
        List<Reservation> newBookList = new ArrayList<Reservation>();
        
        for(Reservation list : bookList) {
            if(list.getStatus() == 'H') {
                newBookList.add(list);
            }
        }
        
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            if(id != null) {
                long bookid = Long.parseLong(id);
                Reservation r = null;
                for(Reservation list: bookList) {
                    if(list.getId() == bookid) {
                        r = list;
                        break;
                    }
                }
                r.setStatus('I');
                reservationFacade.edit(r);
                
                Room room = r.getRoom();                
                room.setStatus('O');
                roomFacade.edit(room);
                
                request.getRequestDispatcher("fronthome.jsp").include(request, response);
                out.println("<br><br>");
                out.println("Customer checked-in successful! Room status have been changed from CLEANED to OCCUPIED!");
            }
            else {
                request.getRequestDispatcher("fronthome.jsp").include(request, response);
                out.println("<br><br>");

                out.println("<table border='1' width='100%'>");
                out.println("<tr>");
                out.println("<th>Customer NRIC</th>");
                out.println("<th>Customer Name</th>");
                out.println("<th>Room Name</th>");
                out.println("<th>Booking Date</th>");
                out.println("<th>Check-in Date</th>");
                out.println("<th>Check-out Date</th>");
                out.println("<th>Action</th>");
                out.println("</tr>");

                for (Reservation list : newBookList) {
                    out.println("<tr>");
                    out.println("<td>" + list.getCust().getNric() + "</td>");
                    out.println("<td>" + list.getCust().getName().toUpperCase() + "</td>");
                    out.println("<td>" + list.getRoom().getName().toUpperCase() + "</td>");
                    out.println("<td>" + list.getBook_date() + "</td>");
                    out.println("<td>" + list.getCheck_in_date() + "</td>");
                    out.println("<td>" + list.getCheck_out_date() + "</td>");
                    out.println("<td><a href='Checkin?id="+ list.getId() +"'>Check-in</a></td>");
                    out.println("</tr>");
                }

                out.println("</table>");
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
