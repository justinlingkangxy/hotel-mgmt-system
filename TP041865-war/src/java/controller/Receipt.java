/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Payment;
import model.PaymentFacade;

/**
 *
 * @author TP041865
 */
@WebServlet(name = "Receipt", urlPatterns = {"/Receipt"})
public class Receipt extends HttpServlet {

    @EJB
    private PaymentFacade paymentFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        Long id = Long.parseLong(request.getParameter("id"));
        List<Payment> payList = paymentFacade.findAll();
        Payment p = null;
        for(Payment list: payList) {
            if(list.getId() == id) {
                p = list;
                break;
            }
        }
        
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            request.getRequestDispatcher("report.jsp").include(request, response);
            out.println("<br><br>");
            
            out.println("<table border='1' cellpadding='5' cellspacing='0'>");
                out.println("<tr>");
                    out.println("<td colspan='2'>Receipt ID: "+p.getId()+"</td>");
                out.println("</tr>");
                out.println("<tr>");
                    out.println("<td colspan='2'>Date Paid: "+p.getDate_paid()+"</td>");
                out.println("</tr>");
                out.println("<tr>");
                    out.println("<td>Customer:<br>NRIC: "+p.getR().getCust().getNric()+"<br>Name: "+p.getR().getCust().getName().toUpperCase()+"</td>");
                    out.println("<td>Issuer:<br>NRIC: "+p.getS().getNric()+"<br>Name: "+p.getS().getName().toUpperCase()+"</td>");
                out.println("</tr>");
                out.println("<tr>");
                    out.println("<td colspan='2' align='right'>Total Payment: RM"+p.getPaid()+"</td>");
                out.println("</tr>");
            out.println("</table>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
