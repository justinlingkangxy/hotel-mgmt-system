/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Staff;
import model.StaffFacade;

/**
 *
 * @author TP041865
 */
@WebServlet(name = "Updstaff", urlPatterns = {"/Updstaff"})
public class Updstaff extends HttpServlet {

    @EJB
    private StaffFacade staffFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String act = request.getParameter("act");
        long id = Long.parseLong(request.getParameter("id"));
        
        Staff s = null;
        
        List<Staff> staffList = staffFacade.findAll();
        
        for (Staff list : staffList) {
            if(list.getId() == id) {
                s = list;
                break;
            }
        }
        
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            if(act != null && act.equals("ups")) {
                String nric = request.getParameter("nric");
                String name = request.getParameter("name").toUpperCase();
                String email = request.getParameter("email");
                String addr = request.getParameter("addr").toUpperCase();
                String city = request.getParameter("city").toUpperCase();
                String state = request.getParameter("state").toUpperCase();
                String pwd = request.getParameter("pwd");
                int phone = Integer.parseInt(request.getParameter("phone"));
                char gender = Character.toUpperCase(request.getParameter("gender").charAt(0));
                char role = Character.toUpperCase(request.getParameter("role").charAt(0));
                
                s.setNric(nric);
                s.setName(name);
                s.setPhone(phone);
                s.setEmail(email);
                s.setAddr(addr);
                s.setCity(city);
                s.setState(state);
                s.setPwd(pwd);
                s.setGender(gender);
                s.setRole(role);
                staffFacade.edit(s);
                
                request.getRequestDispatcher("mnghome.jsp").include(request, response);
                out.println("<br><br>");
                out.println("Update successfully!");
            }
            else {
                request.getRequestDispatcher("mnghome.jsp").include(request, response);
                out.println("<br><br>");
                
                out.println("<form action='Updstaff' method='POST'>");
                    out.println("<table>");
                        out.println("<tr>");
                            out.println("<td>");
                                out.println("NRIC:");
                            out.println("</td>");
                            out.println("<td>");
                                out.println("<input type='text' name='nric' size='20' maxlength='12' required value='"+s.getNric()+"' > *without \"-\"");
                            out.println("</td>");
                        out.println("</tr>");
                        out.println("<tr>");
                            out.println("<td>");
                                out.println("Name:");
                            out.println("</td>"); 
                            out.println("<td>");
                                out.println("<input type='text' name='name' size='20' required value='"+s.getName().toUpperCase()+"' >");
                            out.println("</td>");
                        out.println("</tr>");
                        out.println("<tr>");
                            out.println("<td>");
                                out.println("Gender:");
                            out.println("</td>");
                            out.println("<td>");
                                out.println("<select name='gender'>");
                                    if(s.getGender() == 'M') {
                                        out.println("<option value='M' selected='selected'>Male</option>");
                                        out.println("<option value='F'>Female</option>");
                                    }
                                    else {
                                        out.println("<option value='M'>Male</option>");
                                        out.println("<option value='F' selected='selected'>Female</option>");
                                    }
                                out.println("</select>");
                            out.println("</td>");
                        out.println("</tr>");
                        out.println("<tr>");
                            out.println("<td>");
                                out.println("Phone (+60):");
                            out.println("</td>");
                            out.println("<td>");
                                out.println("<input type='number' name='phone' size='20' required value='"+s.getPhone()+"' >");
                            out.println("</td>");
                        out.println("</tr>");
                        out.println("<tr>");
                            out.println("<td>");
                                out.println("Email:");
                            out.println("</td>"); 
                            out.println("<td>");
                                out.println("<input type='email' name='email' size='25' required value='"+s.getEmail()+"' >");
                            out.println("</td>");
                        out.println("</tr>");
                        out.println("<tr>");
                            out.println("<td>");
                                out.println("Address:");
                            out.println("</td>"); 
                            out.println("<td>");
                                out.println("<input type='text' name='addr' size='25' required value='"+s.getAddr()+"' >");
                            out.println("</td>");
                        out.println("</tr>");
                        out.println("<tr>");
                            out.println("<td>");
                                out.println("City:");
                            out.println("</td>"); 
                            out.println("<td>");
                                out.println("<input type='text' name='city' size='20' required value='"+s.getCity()+"' >");
                            out.println("</td>");
                        out.println("</tr>");
                        out.println("<tr>");
                            out.println("<td>");
                                out.println("State:");
                            out.println("</td>"); 
                            out.println("<td>");
                                out.println("<input type='text' name='state' size='20' required value='"+s.getState()+"' >");
                            out.println("</td>");
                        out.println("</tr>");
                        out.println("<tr>");
                            out.println("<td>");
                                out.println("Password:");
                            out.println("</td>"); 
                            out.println("<td>");
                                out.println("<input type='text' name='pwd' size='20' required value='"+s.getPwd()+"' >");
                            out.println("</td>");
                        out.println("</tr>");
                        out.println("<tr>");
                            out.println("<td>");
                                out.println("Role:");
                            out.println("</td>");
                            out.println("<td>");
                                out.println("<select name='role'>");
                                    switch (s.getRole()) {
                                        case 'M':
                                            out.println("<option value='M' selected='selected'>Manager</option>");
                                            out.println("<option value='R'>Reservation Staff</option>");
                                            out.println("<option value='F'>Front Desk Staff</option>");
                                            out.println("<option value='C'>Cleaning Staff</option>");
                                            break;
                                        case 'R':
                                            out.println("<option value='M'>Manager</option>");
                                            out.println("<option value='R' selected='selected'>Reservation Staff</option>");
                                            out.println("<option value='F'>Front Desk Staff</option>");
                                            out.println("<option value='C'>Cleaning Staff</option>");
                                            break;
                                        case 'F':
                                            out.println("<option value='M'>Manager</option>");
                                            out.println("<option value='R'>Reservation Staff</option>");
                                            out.println("<option value='F' selected='selected'>Front Desk Staff</option>");
                                            out.println("<option value='C'>Cleaning Staff</option>");
                                            break;
                                        case 'C':
                                            out.println("<option value='M'>Manager</option>");
                                            out.println("<option value='R'>Reservation Staff</option>");
                                            out.println("<option value='F'>Front Desk Staff</option>");
                                            out.println("<option value='C' selected='selected'>Cleaning Staff</option>");
                                            break;
                                        default:
                                            break;
                                    }
                                out.println("</select>");
                            out.println("</td>");
                        out.println("</tr>");
                    out.println("</table>");
                    
                    out.println("<input type='hidden' name='id' value='"+s.getId()+"'>");
                    out.println("<input type='hidden' name='act' value='ups'>");

                    out.println("<p><input type='submit' value='Submit'></p>");
                out.println("</form>");
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
