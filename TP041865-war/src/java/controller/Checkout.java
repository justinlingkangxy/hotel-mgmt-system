/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Payment;
import model.PaymentFacade;
import model.Reservation;
import model.ReservationFacade;
import model.Room;
import model.RoomFacade;
import model.Staff;

/**
 *
 * @author TP041865
 */
@WebServlet(name = "Checkout", urlPatterns = {"/Checkout"})
public class Checkout extends HttpServlet {

    @EJB
    private PaymentFacade paymentFacade;

    @EJB
    private RoomFacade roomFacade;

    @EJB
    private ReservationFacade reservationFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String id = request.getParameter("id");
        
        List<Reservation> bookList = reservationFacade.findAll();
        List<Reservation> newBookList = new ArrayList<Reservation>();
        
        for(Reservation list : bookList) {
            if(list.getStatus() == 'I') {
                newBookList.add(list);
            }
        }
        
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            if(id != null) {
                HttpSession session = request.getSession(false);
                Staff s = (Staff)session.getAttribute("who");
                
                long bookid = Long.parseLong(id);
                Reservation r = null;
                for(Reservation list: bookList) {
                    if(list.getId() == bookid) {
                        r = list;
                        break;
                    }
                }
                r.setStatus('O');
                reservationFacade.edit(r);
                
                Room room = r.getRoom();
                room.setStatus('T');
                roomFacade.edit(room);
                
                request.getRequestDispatcher("fronthome.jsp").include(request, response);
                out.println("<br><br>");
                out.println("Customer checked-out successful! Room status have been changed from OCCUPIED to TO BE CLEANED!");
                
                SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
                Date date1 = formatDate.parse(r.getCheck_in_date());
                Date date2 = formatDate.parse(r.getCheck_out_date());
                
                int days = daysBetween(date1, date2);
                double paid = days * 100;
                
                Date date = new Date();
                String book_paid = formatDate.format(date);
                
                Payment p = new Payment(paid, book_paid, r, s);
                paymentFacade.create(p);
                
                out.println("<br><br>");
                out.println("<table border='1' cellpadding='5' cellspacing='0'>");
                    out.println("<tr>");
                        out.println("<td colspan='2'>Receipt ID: "+p.getId()+"</td>");
                    out.println("</tr>");
                    out.println("<tr>");
                        out.println("<td colspan='2'>Date Paid: "+p.getDate_paid()+"</td>");
                    out.println("</tr>");
                    out.println("<tr>");
                        out.println("<td>Customer:<br>NRIC: "+p.getR().getCust().getNric()+"<br>Name: "+p.getR().getCust().getName().toUpperCase()+"</td>");
                        out.println("<td>Issuer:<br>NRIC: "+p.getS().getNric()+"<br>Name: "+p.getS().getName().toUpperCase()+"</td>");
                    out.println("</tr>");
                    out.println("<tr>");
                        out.println("<td colspan='2' align='right'>Total Payment: RM"+p.getPaid()+"</td>");
                    out.println("</tr>");
                out.println("</table>");
            }
            else {
                request.getRequestDispatcher("fronthome.jsp").include(request, response);
                out.println("<br><br>");

                out.println("<table border='1' width='100%'>");
                out.println("<tr>");
                out.println("<th>NRIC</th>");
                out.println("<th>Name</th>");
                out.println("<th>Room Name</th>");
                out.println("<th>Booking Date</th>");
                out.println("<th>Check-in Date</th>");
                out.println("<th>Check-out Date</th>");
                out.println("<th>Action</th>");
                out.println("</tr>");

                for (Reservation list : newBookList) {
                    out.println("<tr>");
                    out.println("<td>" + list.getCust().getNric() + "</td>");
                    out.println("<td>" + list.getCust().getName().toUpperCase() + "</td>");
                    out.println("<td>" + list.getRoom().getName().toUpperCase() + "</td>");
                    out.println("<td>" + list.getBook_date() + "</td>");
                    out.println("<td>" + list.getCheck_in_date() + "</td>");
                    out.println("<td>" + list.getCheck_out_date() + "</td>");
                    out.println("<td><a href='Checkout?id="+ list.getId() +"'>Check-out</a></td>");
                    out.println("</tr>");
                }

                out.println("</table>");
            }
        }
        catch (ParseException ex) {
            Logger.getLogger(Addbook.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public int daysBetween(Date d1, Date d2){
            return (int)( (d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
