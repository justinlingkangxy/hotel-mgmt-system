/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Reservation;
import model.ReservationFacade;
import model.Room;
import model.RoomFacade;

/**
 *
 * @author TP041865
 */
@WebServlet(name = "Updbook", urlPatterns = {"/Updbook"})
public class Updbook extends HttpServlet {

    @EJB
    private RoomFacade roomFacade;

    @EJB
    private ReservationFacade reservationFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String act = request.getParameter("act");
        long id = Long.parseLong(request.getParameter("id"));
        SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
        
        Reservation r = null;

        List<Reservation> bookList = reservationFacade.findAll();

        for (Reservation list : bookList) {
            if(list.getId() == id) {
                r = list;
                break;
            }
        }
        
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            if(act != null && act.equals("upd")) {
                List<Room> roomList = roomFacade.findAll();
                
                String cid = request.getParameter("cid");
                String cod = request.getParameter("cod");

                Date first = formatDate.parse(cid);
                Date last = formatDate.parse(cod);

                List<Room> room = new ArrayList<Room>();

                for(Reservation list : bookList) {
                    Date check_in = formatDate.parse(list.getCheck_in_date());
                    Date check_out = formatDate.parse(list.getCheck_out_date());

                    int f = last.compareTo(check_in);
                    int l = check_out.compareTo(first);

                    if (f>=0 && l>=0) {
                        room.add(list.getRoom());
                    }
                }

                roomList.removeAll(room);

                if(!roomList.isEmpty()) {
                    Room getRoom = roomList.get(0);

                    Reservation reserve = r;
                    r.setCheck_in_date(cid);
                    r.setCheck_out_date(cod);
                    r.setRoom(getRoom);
                    reservationFacade.edit(reserve);

                    request.getRequestDispatcher("reshome.jsp").include(request, response);
                    out.println("<br><br>");
                    out.println("Reservation updated successfully!");
                }
                else {
                    request.getRequestDispatcher("reshome.jsp").include(request, response);
                    out.println("<br><br>");
                    out.println("No room available!");
                }
            }
            else {
                request.getRequestDispatcher("reshome.jsp").include(request, response);
                out.println("<br><br>");
                
                String dt = r.getBook_date();
                Calendar c = Calendar.getInstance();
                c.setTime(formatDate.parse(dt));
                c.add(Calendar.DATE, 1);  // number of days to add
                dt = formatDate.format(c.getTime());  // dt is now the new date
                
                String dt2 = r.getBook_date();
                Calendar c2 = Calendar.getInstance();
                c2.setTime(formatDate.parse(dt2));
                c2.add(Calendar.MONTH, 1);  // number of months to add
                dt2 = formatDate.format(c2.getTime());  // dt2 is now the new date
                
                out.println("<form action='Updbook' method='POST'>");
                    out.println("<table>");
                        out.println("<tr>");
                            out.println("<td>");
                                out.println("Check in date:");
                            out.println("</td>");
                            out.println("<td>");
                                out.println("<input type='date' name='cid' size='20' value='"+r.getCheck_in_date()+"' required min='"+dt+"' max='"+dt2+"'>");
                            out.println("</td>");
                        out.println("</tr>");
                        out.println("<tr>");
                            out.println("<td>");
                                out.println("Check out date:");
                            out.println("</td>"); 
                            out.println("<td>");
                                out.println("<input type='date' name='cod' size='20' value='"+r.getCheck_out_date()+"' required min='"+dt+"'>");
                            out.println("</td>");
                        out.println("</tr>");
                    out.println("</table>");
                    
                    out.println("<input type='hidden' name='id' value='"+r.getId()+"'>");
                    out.println("<input type='hidden' name='act' value='upd'>");

                    out.println("<p><input type='submit' value='Submit'></p>");
                out.println("</form>");
            }
        } catch (ParseException ex) {
            Logger.getLogger(Updbook.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
