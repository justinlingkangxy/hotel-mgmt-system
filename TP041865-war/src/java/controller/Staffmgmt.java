/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Staff;
import model.StaffFacade;

/**
 *
 * @author TP041865
 */
@WebServlet(name = "Staffmgmt", urlPatterns = {"/Staffmgmt"})
public class Staffmgmt extends HttpServlet {

    @EJB
    private StaffFacade staffFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        List<Staff> staffList = staffFacade.findAll();
        
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            request.getRequestDispatcher("mnghome.jsp").include(request, response);
                        
            out.println("<br><br>");
            out.println("Click <a href='addstaff.jsp'>Add Staff</a> to add new staff.");
            out.println("<br><br>");

            out.println("<table border='1' width='100%'>");
            out.println("<tr>");
            out.println("<th>NRIC</th>");
            out.println("<th>Name</th>");
            out.println("<th>Phone (+60)</th>");
            out.println("<th>Email</th>");
            out.println("<th>Gender</th>");
            out.println("<th>Address</th>");
            out.println("<th>Role</th>");
            out.println("<th colspan='2'>Action</th>");
            out.println("</tr>");

            String strGender = "";
            String strRole = "";
            for (Staff list : staffList) {
                if(list.getGender() == 'M') {
                    strGender = "Male";
                }
                else if(list.getGender() == 'F') {
                    strGender = "Female";
                }
                
                switch (list.getRole()) {
                    case 'M':
                        strRole = "Manager";
                        break;
                    case 'R':
                        strRole = "Reservation Staff";
                        break;
                    case 'F':
                        strRole = "Front Desk Staff";
                        break;
                    case 'C':
                        strRole = "Cleaning Staff";
                        break;
                    default:
                        break;
                }
                out.println("<tr>");
                out.println("<td>" + list.getNric() + "</td>");
                out.println("<td>" + list.getName().toUpperCase() + "</td>");
                out.println("<td>" + list.getPhone() + "</td>");
                out.println("<td>" + list.getEmail() + "</td>");
                out.println("<td>" + strGender + "</td>");
                out.println("<td>" + list.getAddr().toUpperCase() + ", " + list.getCity().toUpperCase() + ", " + list.getState().toUpperCase() + "</td>");
                out.println("<td>" + strRole + "</td>");
                out.println("<td><a href='Updstaff?id="+ list.getId() +"'>Update</a></td>");
                out.println("<td><a href='Delstaff?id="+ list.getId() +"'>Delete</a></td>");
                out.println("</tr>");
            }

            out.println("</table>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
