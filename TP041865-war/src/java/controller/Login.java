/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Staff;
import model.StaffFacade;

/**
 *
 * @author TP041865
 */
@WebServlet(name = "Login", urlPatterns = {"/Login"})
public class Login extends HttpServlet {

    @EJB
    private StaffFacade staffFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String nric = request.getParameter("nric");
        String pwd = request.getParameter("pwd");

        Staff s = null;
        
        List<Staff> staffList = staffFacade.findAll();
        
        for (Staff list : staffList) {
            if(list.getNric().equals(nric)) {
                s = list;
                break;
            }
        }
        
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            if(s != null) {
                if(s.getPwd().equals(pwd)) {
                    if(s.getRole() == 'M') {
                        request.getRequestDispatcher("mnghome.jsp").include(request, response);
                    }
                    else if(s.getRole() == 'R') {
                        request.getRequestDispatcher("reshome.jsp").include(request, response);
                    }
                    else if(s.getRole() == 'F') {
                        request.getRequestDispatcher("fronthome.jsp").include(request, response);
                    }
                    else if(s.getRole() == 'C') {
                        request.getRequestDispatcher("cleanhome.jsp").include(request, response);
                    }
                    
                    out.println("<br><br>");
                    out.println("Welcome, " + s.getName().toUpperCase() + "!");
                    HttpSession session = request.getSession();
                    session.setAttribute("who", s);
                }
                else {
                    out.println("Sorry, wrong password!<br><br>");
                    request.getRequestDispatcher("login.jsp").include(request, response);
                }
            }
            else {
                out.println("Sorry, wrong user name!<br><br>");
                request.getRequestDispatcher("login.jsp").include(request, response);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
