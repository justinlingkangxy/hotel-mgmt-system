/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Customer;
import model.CustomerFacade;
import model.Reservation;
import model.ReservationFacade;
import model.Room;
import model.RoomFacade;
import model.Staff;
import model.StaffFacade;

/**
 *
 * @author TP041865
 */
@WebServlet(name = "Createdata", urlPatterns = {"/Createdata"})
public class Createdata extends HttpServlet {

    @EJB
    private StaffFacade staffFacade;

    @EJB
    private RoomFacade roomFacade;

    @EJB
    private ReservationFacade reservationFacade;

    @EJB
    private CustomerFacade customerFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            if(staffFacade.findAll().isEmpty()) {
                Staff s1 = new Staff("000000000001", "Manager", "manager@mail.com", "No.1, Street Road 1", "City", "State", "123", 123456789, 'M', 'M');
                staffFacade.create(s1);
                Staff s2 = new Staff("000000000002", "Receptionist", "receptionist@mail.com", "No.2, Street Road 2", "City", "State", "123", 123456789, 'F', 'R');
                staffFacade.create(s2);
                Staff s3 = new Staff("000000000003", "Front Desk", "front.desk@mail.com", "No.3, Street Road 3", "City", "State", "123", 123456789, 'F', 'F');
                staffFacade.create(s3);
                Staff s4 = new Staff("000000000004", "Cleaner", "cleaner@mail.com", "No.4, Street Road 4", "City", "State", "123", 123456789, 'M', 'C');
                staffFacade.create(s4);
                
                out.println("Default staff data created!");
                out.println("<br><br>");
            }
            if(customerFacade.findAll().isEmpty()) {
                Customer c1 = new Customer("100000000001", "Customer 1", "cust1@mail.com", "No.1A, Street Road 1A", "City", "State", 987654321, 'M');
                customerFacade.create(c1);
                Customer c2 = new Customer("100000000002", "Customer 2", "cust2@mail.com", "No.2B, Street Road 2B", "City", "State", 987654321, 'F');
                customerFacade.create(c2);
                Customer c3 = new Customer("100000000003", "Customer 3", "cust3@mail.com", "No.3C, Street Road 3C", "City", "State", 987654321, 'M');
                customerFacade.create(c3);
                Customer c4 = new Customer("100000000004", "Customer 4", "cust4@mail.com", "No.4D, Street Road 4D", "City", "State", 987654321, 'F');
                customerFacade.create(c4);
                Customer c5 = new Customer("100000000005", "Customer 5", "cust5@mail.com", "No.5E, Street Road 5E", "City", "State", 987654321, 'M');
                customerFacade.create(c5);
                out.println("Default customer data created!");
                out.println("<br><br>");
            }
            if(roomFacade.findAll().isEmpty()) {
                String name;
                for(int i=1; i<=3; i++) {
                    for(int j=1; j<=10; j++) {
                        name = "Floor " + i + " Room " + j;
                        Room room = new Room(name, 'C');
                        roomFacade.create(room);
                    }
                }
                out.println("Default room data created!");
                out.println("<br><br>");
            }
            if(reservationFacade.findAll().isEmpty()) {
                SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
                Date date = new Date();
                String book_date = formatDate.format(date);
                String dt = book_date;
                Calendar c = Calendar.getInstance();
                c.setTime(formatDate.parse(dt));
                c.add(Calendar.DATE, 1);  // number of days to add
                dt = formatDate.format(c.getTime());  // dt is now the new date

                String dt2 = book_date;
                Calendar c2 = Calendar.getInstance();
                c2.setTime(formatDate.parse(dt2));
                c2.add(Calendar.DATE, 5);  // number of months to add
                dt2 = formatDate.format(c2.getTime());  // dt2 is now the new date

                List<Room> room = roomFacade.findAll();
                List<Customer> cust = customerFacade.findAll();
                List<Staff> staff = staffFacade.findAll();
                int j = 0;
                for(int i=0; i<30; i++) {
                    Reservation book = new Reservation(room.get(i), cust.get(j), staff.get(1), book_date, dt, dt2, 'H');
                    reservationFacade.create(book);
                    if((j+1)>=cust.size()) {
                        j = 0;
                    }
                    else {
                        j++;
                    }
                }
                out.println("Default booking data created!");
                out.println("<br><br>");
            }
            
            request.getRequestDispatcher("login.jsp").include(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(Createdata.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
