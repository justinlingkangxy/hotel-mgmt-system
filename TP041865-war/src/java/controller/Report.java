/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Payment;
import model.PaymentFacade;
import model.Reservation;
import model.ReservationFacade;
import model.Room;
import model.RoomFacade;

/**
 *
 * @author TP041865
 */
@WebServlet(name = "Report", urlPatterns = {"/Report"})
public class Report extends HttpServlet {

    @EJB
    private PaymentFacade paymentFacade;

    @EJB
    private RoomFacade roomFacade;

    @EJB
    private ReservationFacade reservationFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        char view = request.getParameter("view").charAt(0);
        
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            request.getRequestDispatcher("report.jsp").include(request, response);
            out.println("<br><br>");
            
            if(view == 'b') {
                List<Reservation> bookList = reservationFacade.findAll();
                int countH = 0;
                int countI = 0;
                int countO = 0;
                
                for(Reservation list : bookList) {
                    switch (list.getStatus()) {
                        case 'H':
                            countH++;
                            break;
                        case 'I':
                            countI++;
                            break;
                        case 'O':
                            countO++;
                            break;
                        default:
                            break;
                    }
                }
                
                out.println("<table frame='box'");
                    out.println("<tr>");
                        out.println("<td>");
                            out.println("Number of customer HAVENT CHECKED IN:");
                        out.println("</td>");
                        out.println("<td>");
                            out.println(countH);
                        out.println("</td>");
                    out.println("</tr>");
                    out.println("<tr>");
                        out.println("<td>");
                            out.println("Number of customer CHECK IN:");
                        out.println("</td>");
                        out.println("<td>");
                            out.println(countI);
                        out.println("</td>");
                    out.println("</tr>");
                    out.println("<tr>");
                        out.println("<td>");
                            out.println("Number of customer CHECK OUT:");
                        out.println("</td>");
                        out.println("<td>");
                            out.println(countO);
                        out.println("</td>");
                    out.println("</tr>");
                out.println("</table>");
                out.println("<br><br>");

                out.println("<table border='1' width='100%'>");
                out.println("<tr>");
                out.println("<th>Customer NRIC</th>");
                out.println("<th>Customer Name</th>");
                out.println("<th>Booking Date</th>");
                out.println("<th>Check-in Date</th>");
                out.println("<th>Check-out Date</th>");
                out.println("<th>Status</th>");
                out.println("</tr>");

                String str_status = "";

                for (Reservation list : bookList) {
                    switch (list.getStatus()) {
                        case 'I':
                            str_status = "Checked-in";
                            break;
                        case 'H':
                            str_status = "Havent Checked-in";
                            break;
                        case 'O':
                            str_status = "Checked-out";
                            break;
                        default:
                            break;
                    }
                    out.println("<tr>");
                    out.println("<td>" + list.getCust().getNric() + "</td>");
                    out.println("<td>" + list.getCust().getName().toUpperCase() + "</td>");
                    out.println("<td>" + list.getBook_date() + "</td>");
                    out.println("<td>" + list.getCheck_in_date() + "</td>");
                    out.println("<td>" + list.getCheck_out_date() + "</td>");
                    out.println("<td>" + str_status + "</td>");
                    out.println("</tr>");
                }

                out.println("</table>");
            }
            else if(view == 'i') {
                List<Payment> payList = paymentFacade.findAll();
                
                double total = 0;
                for (Payment list : payList) {
                    total += list.getPaid();
                }
                out.println("Total income: RM"+total);
                out.println("<br><br>");
                out.println("<table border='1' width='100%'>");
                out.println("<tr>");
                out.println("<th>Receipt ID</th>");
                out.println("<th>Date Payment</th>");
                out.println("<th>Customer NRIC</th>");
                out.println("<th>Customer Name</th>");
                out.println("<th>Issuer NRIC</th>");
                out.println("<th>Issuer Name</th>");
                out.println("<th>Total Payment (RM)</th>");
                out.println("<th>View Receipt</th>");
                out.println("</tr>");
                
                for (Payment list : payList) {
                    out.println("<tr>");
                    out.println("<td>" + list.getId() + "</td>");
                    out.println("<td>" + list.getDate_paid() + "</td>");
                    out.println("<td>" + list.getR().getCust().getNric() + "</td>");
                    out.println("<td>" + list.getR().getCust().getName().toUpperCase() + "</td>");
                    out.println("<td>" + list.getS().getNric() + "</td>");
                    out.println("<td>" + list.getS().getName().toUpperCase() + "</td>");
                    out.println("<td>" + list.getPaid() + "</td>");
                    out.println("<td><a href='Receipt?id="+ list.getId() +"'>View</a></td>");
                    out.println("</tr>");
                }

                out.println("</table>");
            }
            else if(view == 'r') {
                List<Room> roomList = roomFacade.findAll();
                int countC = 0;
                int countT = 0;
                int countO = 0;
                
                for(Room list : roomList) {
                    switch (list.getStatus()) {
                        case 'C':
                            countC++;
                            break;
                        case 'T':
                            countT++;
                            break;
                        case 'O':
                            countO++;
                            break;
                        default:
                            break;
                    }
                }
                
                out.println("<table frame='box'");
                    out.println("<tr>");
                        out.println("<td>");
                            out.println("Number of room CLEANED:");
                        out.println("</td>");
                        out.println("<td>");
                            out.println(countC);
                        out.println("</td>");
                    out.println("</tr>");
                    out.println("<tr>");
                        out.println("<td>");
                            out.println("Number of room TO BE CLEANED:");
                        out.println("</td>");
                        out.println("<td>");
                            out.println(countT);
                        out.println("</td>");
                    out.println("</tr>");
                    out.println("<tr>");
                        out.println("<td>");
                            out.println("Number of room OCCUPIED:");
                        out.println("</td>");
                        out.println("<td>");
                            out.println(countO);
                        out.println("</td>");
                    out.println("</tr>");
                out.println("</table>");
                out.println("<br><br>");

                out.println("<table border='1' width='100%'>");
                out.println("<tr>");
                out.println("<th>Room Name</th>");
                out.println("<th>Status</th>");
                out.println("</tr>");

                String str_status = "";

                for (Room list : roomList) {
                    switch (list.getStatus()) {
                        case 'C':
                            str_status = "Cleaned";
                            break;
                        case 'T':
                            str_status = "To be cleaned";
                            break;
                        case 'O':
                            str_status = "Occupied";
                            break;
                        default:
                            break;
                    }
                    out.println("<tr>");
                    out.println("<td>" + list.getName().toUpperCase() + "</td>");
                    out.println("<td>" + str_status + "</td>");
                    out.println("</tr>");
                }

                out.println("</table>");
            }
        }
    }

    public static class WeekComparator implements Comparator<Date> {

        @Override
        public int compare(Date o1, Date o2) {
            int result = getWeekOfYear(o1) - getWeekOfYear(o2);
            if (result == 0) {
                result = o1.compareTo(o2);
            }
            return result;
        }

    }

    protected static int getWeekOfYear(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.WEEK_OF_YEAR);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
