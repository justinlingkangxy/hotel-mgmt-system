/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Reservation;
import model.ReservationFacade;

/**
 *
 * @author TP041865
 */
@WebServlet(name = "Bookmgmt", urlPatterns = {"/Bookmgmt"})
public class Bookmgmt extends HttpServlet {

    @EJB
    private ReservationFacade reservationFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        List<Reservation> bookList = reservationFacade.findAll();
        
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            request.getRequestDispatcher("reshome.jsp").include(request, response);
                        
            out.println("<br><br>");
            out.println("Click <a href='Addbook'>here</a> to add new booking.");
            out.println("<br><br>");

            out.println("<table border='1' width='100%'>");
            out.println("<tr>");
            out.println("<th>Customer NRIC</th>");
            out.println("<th>Customer Name</th>");
            out.println("<th>Booking Date</th>");
            out.println("<th>Check-in Date</th>");
            out.println("<th>Check-out Date</th>");
            out.println("<th>Status</th>");
            out.println("<th colspan='2'>Action</th>");
            out.println("</tr>");

            String str_status = "";
            
            for (Reservation list : bookList) {
                switch (list.getStatus()) {
                    case 'I':
                        str_status = "Checked-in";
                        break;
                    case 'H':
                        str_status = "Havent Checked-in";
                        break;
                    case 'O':
                        str_status = "Checked-out";
                        break;
                    default:
                        break;
                }
                out.println("<tr>");
                out.println("<td>" + list.getCust().getNric() + "</td>");
                out.println("<td>" + list.getCust().getName().toUpperCase() + "</td>");
                out.println("<td>" + list.getBook_date() + "</td>");
                out.println("<td>" + list.getCheck_in_date() + "</td>");
                out.println("<td>" + list.getCheck_out_date() + "</td>");
                out.println("<td>" + str_status + "</td>");
                out.println("<td><a href='Updbook?id="+ list.getId() +"'>Update</a></td>");
                out.println("<td><a href='Delbook?id="+ list.getId() +"'>Delete</a></td>");
                out.println("</tr>");
            }

            out.println("</table>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
