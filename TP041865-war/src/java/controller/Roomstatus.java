/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Reservation;
import model.Room;
import model.RoomFacade;

/**
 *
 * @author TP041865
 */
@WebServlet(name = "Roomstatus", urlPatterns = {"/Roomstatus"})
public class Roomstatus extends HttpServlet {

    @EJB
    private RoomFacade roomFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String id = request.getParameter("id");
        
        List<Room> roomList = roomFacade.findAll();
        
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            if(id != null) {
                long bookid = Long.parseLong(id);
                Room r = null;
                for(Room list: roomList) {
                    if(list.getId() == bookid) {
                        r = list;
                        break;
                    }
                }
                r.setStatus('C');
                roomFacade.edit(r);
                
                request.getRequestDispatcher("cleanhome.jsp").include(request, response);
                out.println("<br><br>");
                out.println("Room status have been changed from TO BE CLEANED to CLEANED!");
            }
            else {
                request.getRequestDispatcher("cleanhome.jsp").include(request, response);
                out.println("<br><br>");

                out.println("<table border='1' width='100%'>");
                out.println("<tr>");
                out.println("<th>Room Name</th>");
                out.println("<th>Status</th>");
                out.println("<th>Action</th>");
                out.println("</tr>");

                List<Room> newRoomList = new ArrayList<Room>();
                for(Room list : roomList) {
                    if(list.getStatus() == 'T') {
                        newRoomList.add(list);
                    }
                }
                
                String strStatus = "";
                for (Room list : newRoomList) {
                    switch (list.getStatus()) {
                        case 'C':
                            strStatus = "Cleaned";
                            break;
                        case 'T':
                            strStatus = "To Be Cleaned";
                            break;
                        case 'O':
                            strStatus = "Occupied";
                            break;
                        default:
                            break;
                    }
                    out.println("<tr>");
                    out.println("<td>" + list.getName().toUpperCase() + "</td>");
                    out.println("<td>" + strStatus + "</td>");
                    out.println("<td><a href='Roomstatus?id="+ list.getId() +"'>Change status</a></td>");
                    out.println("</tr>");
                }

                out.println("</table>");
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
