/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Customer;
import model.CustomerFacade;
import model.Reservation;
import model.ReservationFacade;
import model.Room;
import model.RoomFacade;
import model.Staff;

/**
 *
 * @author TP041865
 */
@WebServlet(name = "Addbook", urlPatterns = {"/Addbook"})
public class Addbook extends HttpServlet {

    @EJB
    private RoomFacade roomFacade;

    @EJB
    private ReservationFacade reservationFacade;

    @EJB
    private CustomerFacade customerFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String act = request.getParameter("act");
        SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String book_date = formatDate.format(date);
        
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            if(act != null && act.equals("add")) {
                HttpSession session = request.getSession(false);
                Staff s = (Staff)session.getAttribute("who");
                long cust_id = Long.parseLong(request.getParameter("cust_id"));

                List<Reservation> bookList = reservationFacade.findAll();
                List<Room> roomList = roomFacade.findAll();
                List<Customer> custList = customerFacade.findAll();

                String cid = request.getParameter("cid");
                String cod = request.getParameter("cod");

                Date first = formatDate.parse(cid);
                Date last = formatDate.parse(cod);

                List<Room> r = new ArrayList<Room>();

                for(Reservation list : bookList) {
                    Date check_in = formatDate.parse(list.getCheck_in_date());
                    Date check_out = formatDate.parse(list.getCheck_out_date());

                    int f = last.compareTo(check_in);
                    int l = check_out.compareTo(first);

                    if (f>=0 && l>=0) {
                        r.add(list.getRoom());
                    }
                }

                roomList.removeAll(r);

                if(!roomList.isEmpty()) {
                    Room room = roomList.get(0);

                    Customer cust = null;
                    for(Customer list : custList) {
                        if(list.getId() == cust_id) {
                            cust = list;
                        }
                    }

                    Reservation reserve = new Reservation(room, cust, s, book_date, cid, cod, 'H');
                    reservationFacade.create(reserve);

                    request.getRequestDispatcher("reshome.jsp").include(request, response);
                    out.println("<br><br>");
                    out.println("Reservation done!");
                }
                else {
                    request.getRequestDispatcher("reshome.jsp").include(request, response);
                    out.println("<br><br>");
                    out.println("No room available!");
                }
            }
            else {
                request.getRequestDispatcher("reshome.jsp").include(request, response);
                out.println("<br><br>");

                List<Customer> customerList = customerFacade.findAll();
                String dt = book_date;
                Calendar c = Calendar.getInstance();
                c.setTime(formatDate.parse(dt));
                c.add(Calendar.DATE, 1);  // number of days to add
                dt = formatDate.format(c.getTime());  // dt is now the new date
                
                String dt2 = book_date;
                Calendar c2 = Calendar.getInstance();
                c2.setTime(formatDate.parse(dt2));
                c2.add(Calendar.MONTH, 1);  // number of months to add
                dt2 = formatDate.format(c2.getTime());  // dt2 is now the new date

                out.println("<form action='Addbook' method='POST'>");
                    out.println("<table>");
                        out.println("<tr>");
                            out.println("<td>");
                                out.println("Customer:");
                            out.println("</td>");
                            out.println("<td>");
                                out.println("<select name='cust_id'>");
                                    for(Customer list : customerList) {
                                        out.println("<option value='"+list.getId()+"'>"+list.getNric()+" | "+list.getName().toUpperCase()+"</option>");
                                    }
                                out.println("</select>");
                            out.println("</td>");
                        out.println("</tr>");
                        out.println("<tr>");
                            out.println("<td>");
                                out.println("Check in date:");
                            out.println("</td>");
                            out.println("<td>");
                                out.println("<input type='date' name='cid' size='20' required min='"+dt+"' max='"+dt2+"'>");
                            out.println("</td>");
                        out.println("</tr>");
                        out.println("<tr>");
                            out.println("<td>");
                                out.println("Check out date:");
                            out.println("</td>"); 
                            out.println("<td>");
                                out.println("<input type='date' name='cod' size='20' required min='"+dt+"'>");
                            out.println("</td>");
                        out.println("</tr>");
                    out.println("</table>");

                    out.println("<input type='hidden' name='act' value='add'>");

                    out.println("<p><input type='submit' value='Submit'></p>");

                    out.println("Cannot find customer? Click <a href='addcust.jsp'>here</a> to add new customer.");
                out.println("</form>");
            }
        }
        catch (ParseException ex) {
            Logger.getLogger(Addbook.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
