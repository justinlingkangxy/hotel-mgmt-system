<%-- 
    Document   : addstaff
    Created on : Sep 17, 2017, 6:09:03 PM
    Author     : TP041865
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add Staff Page</title>
    </head>
    <body>
        <jsp:include page="mnghome.jsp" />
        <br/><br/>
        <form action="Addstaff" method="POST">
            <table>
                <tr>
                    <td>
                        NRIC:
                    </td> 
                    <td>
                        <input type="text" name="nric" size="20" maxlength="12" required> *without "-"
                    </td>
                </tr>
                <tr>
                    <td>
                        Name:
                    </td> 
                    <td>
                        <input type="text" name="name" size="20" required>
                    </td>
                </tr>
                <tr>
                    <td>
                        Gender:
                    </td>
                    <td>
                        <select name="gender">
                            <option value="M">Male</option>
                            <option value="F">Female</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        Phone (+60):
                    </td> 
                    <td>
                        <input type="number" name="phone" size="20" required>
                    </td>
                </tr>
                <tr>
                    <td>
                        Email:
                    </td> 
                    <td>
                        <input type="email" name="email" size="25" required>
                    </td>
                </tr>
                <tr>
                    <td>
                        Address:
                    </td> 
                    <td>
                        <input type="text" name="addr" size="25" required>
                    </td>
                </tr>
                <tr>
                    <td>
                        City:
                    </td> 
                    <td>
                        <input type="text" name="city" size="20" required>
                    </td>
                </tr>
                <tr>
                    <td>
                        State:
                    </td> 
                    <td>
                        <input type="text" name="state" size="20" required>
                    </td>
                </tr>
                <tr>
                    <td>
                        Password:
                    </td> 
                    <td>
                        <input type="text" name="pwd" size="20" required>
                    </td>
                </tr>
                <tr>
                    <td>
                        Role:
                    </td>
                    <td>
                        <select name="role">
                            <option value="M">Manager</option>
                            <option value="R">Reservation Staff</option>
                            <option value="F">Front Desk Staff</option>
                            <option value="C">Cleaning Staff</option>
                        </select>
                    </td>
                </tr>
            </table>
            <p>
                <input type="submit" value="Submit">
            </p>
        </form>
    </body>
</html>
